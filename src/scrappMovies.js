var Crawler = require("crawler");
const mongoose = require('mongoose');
const genreGroups = require("./getGenreGroups");
const movies = require('./getMovies');
const movieSchema = require('./schemas/videoId');
const genreGroupSchema = require('./schemas/genresGroup');

mongoose.connect('mongodb://sky:sky123@ds050739.mlab.com:50739/amazon_movies_id', {useNewUrlParser: true});

//contador filmes
var movieAmount = 0;
var moviesList = [];
var genreGroupsList = [];
var movieAsinLocalList = [];

//urlList
var urlList = [];
var needle = 0;

//get the movies values
var c = new Crawler({
    maxConnections : 3,
    // This will be called for each crawled page
    callback : function (error, res, done) {
        if(error){
            console.log(error);
        }else{
            var $ = res.$;
            scrappPage($, checkNext);
            done();
        }
    }
});

//inside page movie data list scrapp function
function scrappPage(dom, callback) {
    var asinList = [];
    var nextPage = false;

    //exits function in case there's notitles to scrapp
    if(dom('#noResultsTitle').length > 0) {
        console.log("Não existe match ente este filtro de Sub-Gênero e Data, tentando o proximo Sub-Gênero...");
        needle++;
        beginScrapp(urlList);
    } else { //in case there's pagination to do, keep on paginating the filter
        //li info (ASIN)
        dom('.s-result-item.celwidget').each(function(i,a) {
            asinList.push(a.attribs['data-asin']);
        });

        //a link info (movie data)
        dom('.a-link-normal.s-access-detail-page.s-color-twister-title-link.a-text-normal').each(function(i,a) {
            var movieValues = new movieSchema({
                status: 'New',
                name:  a.attribs.title,
                full_url:   a.attribs.href,
                asin: asinList[i],
                genre: genreGroupsList[needle].genreName,
                page: genreGroupsList[needle].url
            }); 

            //save movie info into movie model - - check/update local list of asins
            if(moviesList.indexOf(asinList[i]) == -1  && movieAsinLocalList.indexOf(asinList[i]) == -1) {
                movieAsinLocalList.push(asinList[i]);
                movieValues.save();
                console.log("Filme ["+ a.attribs.title+ "] salvo com sucesso! [QTD: "+movieAmount+"]");
                movieAmount++;
            } else {
                console.log("Filme ["+ a.attribs.title+ "] já existe na base.  [QTD: "+movieAmount+"]");
            }
        });
        
        //update all pages from a single subcategory field (only first page for each page)
        if(dom("#s-result-count").text().substring(0,2) == '1-') {
            textValue = dom(".pagnDisabled").text();
            console.log('Updated All Pages:'+textValue);
            updateLastScrapp('genre',genreGroupsList[needle], 'allPages', parseInt(textValue));
        }

        //check if there's a next page button to scrapp
        if(dom('#pagnNextLink')[0] != undefined) {
            nextPage = dom("#pagnNextLink")[0].attribs.href;
        } else if (dom('.a-last').text().substr(0,5)=="Sorry") { //blocked by robot
            updateLastScrapp('genre',genreGroupsList[needle], 'lastPageUrl', 'Blocked'); 
        }
        callback(nextPage); 
   }
}

//interative internal page scrapp function
function checkNext(page) {
    if(page) {
        
        //updates last page of sub genre (LOGICA DE UPDATE DO LAST PAGE DE CADA SUB-GENERO)
        //lastPage = page.substring(page.indexOf("page=")+5, page.indexOf('&ie'));
        //updateLastScrapp('genre',genreGroupsList[needle], 'lastPage', parseInt(lastPage));
        //updateLastScrapp('genre',genreGroupsList[needle], 'lastPageUrl', page);
        
        //check next page
        c.queue("http://www.amazon.com"+page);
    } else {
        //updates end update of subgenre
        updateLastScrapp('genre',genreGroupsList[needle], 'lastUpdatedEnd');
        //grava o update no last_up_end do genre
        console.log("Não existem mais páginas do Sub-Gênero ["+genreGroupsList[needle].genreName+"] para verificar, passando para o próximo Sub-Gênero...");
        //send to next page
        needle++;
        beginScrapp(urlList);
    }
}

//iterative page scrapp function
function beginScrapp(list) {
    if (typeof list[needle] != 'undefined') {
        //updates begin update of subgenre
        updateLastScrapp('genre',genreGroupsList[needle], 'lastUpdatedBegin');
        //crawl the genre
        c.queue(list[needle]);

        //LOGICA DE NAO PASSAR POR SUB-GENEROS CUJA PAGINAÇÃO TENHA SIDO CONCLUÍDA
        /*if(genreGroupsList[needle].allPages != genreGroupsList[needle].lastPage) {
            c.queue(list[needle]);
        } else { //skip to next unfinished genre
            needle++;
            c.queue(list[needle]);
        }*/

    } else {
        console.log("Fim da lista de Canais");
    }
}

//function that updates last date of modify scrapp markers
function updateLastScrapp(modelType, doc, field, value = null) {
    var model;
    //in case of specific value
    if(value == null) {
        value = new Date().toLocaleString();
    }
    if(modelType == 'genre') {
        model = genreGroupSchema;
    } else {
        model = movieSchema;
    }

    //update model
    model.updateOne(
        { _id: doc._id },
        { $set: { [field] : value }
    }, function(err,doc){});
}

//MAIN FUNCTION
genreGroups.then(function(results) {
    //MAIN FUNCTION
    results.forEach(genreGroup => {
        genreGroupsList.push(genreGroup);
        urlList.push(genreGroup.url);
    });
    beginScrapp(urlList);
    //get all movies asin's
    movies.then(function(res) {
        res.forEach(movie => {
            moviesList.push(movie.asin);
        });
        //first scrapp
        beginScrapp(urlList);
    });
}); 