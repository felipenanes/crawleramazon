var Crawler = require("crawler");
const mongoose = require('mongoose');
const genreGroup = require('./schemas/genresGroup');

/*mongoose.connection.on('open', function () {
  mongoose.connection.db.listCollections({name: 'genresGroup'})
      .next(function(err, exists) {
          if (!exists) {
            genreGroup.create({name: "", url: ""}, function(err, doc) {});
          } else {
            //erase previous urls
            genreGroup.collection.drop();
          }
      });
});*/

//erase previous urls
genreGroup.collection.drop();

mongoose.connect('mongodb://localhost:27017/test', {useNewUrlParser: true});

//gener settings
var genreList = [];

//urlList
var urlList = [];
var needle = 0;

//set the crawler ready for use
var c = new Crawler({
    maxConnections : 10,
    // This will be called for each crawled page
    callback : function (error, res, done) {
        if(error){
            console.log(error);
        }else{
            var $ = res.$;
            getFilteredUrls($);
            done();
        }
    }
});

//inside page movie data list scrapp function
function getFilteredUrls(dom) {
    var subGenreList = [];
    var subGenreDataList = [];
    var fullfilteredUrls = [];

    //get all sug-genders to scrapp
    var allUrlFilters = dom('.a-row.a-expander-container.a-expander-extend-container .a-list-item .a-declarative').toArray();
    allUrlFilters.forEach(filter => {
        if(filter.attribs['data-s-ref-selected'].substr(9).search('sr_nr_p_n_feature_four_bro_') != -1) {
            filterUrl = filter.attribs['data-s-ref-selected'].substr(9).slice(0,-2);
            let filterUrlNew = filterUrl.replace(/amp;/g, '');
            let name = filter.lastChild.lastChild.children[2].children[0].children[0].data;
            filterUrlNew = [name, filterUrlNew.substring(0, filterUrlNew.search("&bbn"))];
            //push to sub-genres local list
            subGenreList.push(filterUrlNew);
        }
    });

    //get all data ranges from that gender 
    var allDataFilters = dom('.a-row.a-expander-container.a-expander-extend-container .a-list-item .a-declarative').toArray();
    allDataFilters.forEach(filter => {
        if(filter.attribs['data-s-ref-selected'].substr(9).search('sr_nr_p_n_feature_three_br_') != -1) {
            filterUrl = filter.attribs['data-s-ref-selected'].substr(9).slice(0,-2);
            let filterUrlNew = filterUrl.replace(/amp;/g, '');
            let date = filter.lastChild.lastChild.children[2].children[0].children[0].data;
            filterUrlNew = [date, filterUrlNew.substring(0, filterUrlNew.search("&bbn"))];
            //push to sub-genres local list
            subGenreDataList.push(filterUrlNew);
        }
    });
    //integrates the genre filter url with the date filter url from that gender
    subGenreList.forEach(subGender => {
        actualSubGender =  "https://www.amazon.com/s/"+subGender[1];
        subGenreDataList.forEach(subGenDataFilter => {
            fullSubGenderUlr = actualSubGender + subGenDataFilter[1].substring(subGenDataFilter[1].lastIndexOf("%2Cp"));
            genderInsertion = [
                subGender[0],
                subGenDataFilter[0],
                fullSubGenderUlr,
            ];
            fullfilteredUrls.push(genderInsertion);
        });
    });
    //saves the url list for that genre in the mongoDB
    fullfilteredUrls.forEach(filteredUrl => {
        //save genderGroup into mongoDB 
        var genreValues = new genreGroup({
            genreId : genreList[needle]._id,
            genreName : genreList[needle].name,
            url : filteredUrl[2],
            subGendName: filteredUrl[0],
            dataRange: filteredUrl[1],
            lastUpdatedBegin: null,
            lastUpdatedEnd: null,
            allPages: null,
            lastPage: null,
            lastPageUrl: null
        });
        genreValues.save(function(err) {
            if(err) 
                console.log(err);
        });
    });
    
    console.log("Urls do Gênero "+genreList[needle].name+" salvos com sucesso!");

    needle++;
    beginScrapp(urlList);
}

//iterative page scrapp function
function beginScrapp(list) {
    if (typeof list[needle] != 'undefined' /*&& needle <= 0*/) {
        //crawl the genre
        c.queue(list[needle]);
    } else {
        console.log("Fim da lista de Gêneros");
    }
}

module.exports = beginScrapp;