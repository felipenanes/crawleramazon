const mongoose = require('mongoose');
// const winston = require('../log/winston');

if (process.env.DEBUG) mongoose.set('debug', true);

const dbURI = process.env.DBURI;
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
mongoose.connect(dbURI,
	{ useNewUrlParser: true }
);

const db = mongoose.connection;
db.on('connected', () => {
//   winston.debug('Conectou ao banco de dados:', mongoose.connection);
});

db.on('error', (err) => {
	console.error('Erro: ', err);
//   winston.error('Mongoose informa um erro na conexão: %o', err);
});

db.on('disconnected', () => {
//   winston.debug('Mongoose conexão terminada');
});

process.on('SIGINT', () => {
	db.close(() => {
		console.log('Terminando a aplicação');
		process.exit(0);
	});
});

module.exports = mongoose;
